package demo.moengageevent;

import android.app.Application;
import android.util.Log;

import com.moe.pushlibrary.MoEHelper;
import com.moengage.core.MoEngage;
import com.moengage.push.PushManager;

/**
 * @author sonal
 */
public class MoEngageApplication extends Application implements PushManager.OnTokenReceivedListener {

    @Override
    public void onCreate() {
        super.onCreate();

        // this is the instance of the application class
        MoEngage moEngage = new MoEngage.Builder(this, BuildConfig.moengageAppId)
                .setSenderId(BuildConfig.fcmSenderId)
                .setNotificationSmallIcon(R.mipmap.ic_launcher)
                .setNotificationLargeIcon(R.mipmap.ic_launcher_round)
                .build();
        MoEngage.initialise(moEngage);

        MoEHelper.getInstance(getApplicationContext()).setExistingUser(false);

        PushManager.getInstance().setTokenObserver(this);
    }

    @Override
    public void onTokenReceived(String token) {
        //save token for your use
        Log.d("MoEngageApplication", "onTokenReceived: "+token);
    }
}
