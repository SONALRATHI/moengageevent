package demo.moengageevent;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.moe.pushlibrary.MoEHelper;

import static demo.moengageevent.EventUtils.CAMERA_CLICK_ACTION;
import static demo.moengageevent.EventUtils.GALLERY_CLICK_ACTION;
import static demo.moengageevent.EventUtils.HOME_PAGE;
import static demo.moengageevent.EventUtils.HOME_SCREEN_NAME;
import static demo.moengageevent.EventUtils.LOGOUT_CLICK_ACTION;
import static demo.moengageevent.EventUtils.SEND_CLICK_ACTION;
import static demo.moengageevent.EventUtils.SHARE_CLICK_ACTION;
import static demo.moengageevent.EventUtils.SLIDESHOW_CLICK_ACTION;
import static demo.moengageevent.EventUtils.TOOLS_CLICK_ACTION;
import static demo.moengageevent.EventUtils.clickNavEvent;
import static demo.moengageevent.EventUtils.trackScreenLoadEvent;

/**
 * @author sonal
 */
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        trackScreenLoadEvent(this, HOME_PAGE);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        String name = getIntent().getStringExtra("name");
        String email = getIntent().getStringExtra("email");
        TextView mesaage = findViewById(R.id.userNameText);
        mesaage.setText("Hi "+name+" MoEngage welcome you");

        Snackbar.make(findViewById(R.id.drawer_layout), "You are loggen in with "+email, Snackbar.LENGTH_LONG).show();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            logout();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_camera:
                clickNavEvent(this, CAMERA_CLICK_ACTION, HOME_SCREEN_NAME);
                break;
            case R.id.nav_gallery:
                clickNavEvent(this, GALLERY_CLICK_ACTION, HOME_SCREEN_NAME);
                break;
            case R.id.nav_manage:
                clickNavEvent(this, TOOLS_CLICK_ACTION, HOME_SCREEN_NAME);
                break;
            case R.id.nav_send:
                clickNavEvent(this, SEND_CLICK_ACTION, HOME_SCREEN_NAME);
                break;
            case R.id.nav_share:
                clickNavEvent(this, SHARE_CLICK_ACTION, HOME_SCREEN_NAME);
                break;
            case R.id.nav_slideshow:
                clickNavEvent(this, SLIDESHOW_CLICK_ACTION, HOME_SCREEN_NAME);
                break;
            case R.id.logout:
                logout();
                break;
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void logout() {
        clickNavEvent(this, LOGOUT_CLICK_ACTION, HOME_SCREEN_NAME);
        MoEHelper.getInstance(getApplicationContext()).logoutUser();
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }
}
