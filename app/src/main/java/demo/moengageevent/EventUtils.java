package demo.moengageevent;

import android.content.Context;

import com.moe.pushlibrary.MoEHelper;
import com.moe.pushlibrary.PayloadBuilder;

import java.util.UUID;

/**
 * @author sonal
 */
public class EventUtils {

    static final String LOGIN_SCREEN_NAME = "login_page";
    static final String HOME_SCREEN_NAME = "home_page";

    static final String HOME_PAGE = "home_page_load_event";
    static final String LOGIN_PAGE = "login_page_load_event";

    static final String LOGIN_CLICK_ACTION = "login";
    static final String CAMERA_CLICK_ACTION = "camera";
    static final String GALLERY_CLICK_ACTION = "gallery";
    static final String SLIDESHOW_CLICK_ACTION = "slideshow";
    static final String TOOLS_CLICK_ACTION = "tools";
    static final String SHARE_CLICK_ACTION = "share";
    static final String SEND_CLICK_ACTION = "send";
    static final String LOGOUT_CLICK_ACTION = "logout";

    static void setLoginUserAttributes(Context context, String email, String name) {
        MoEHelper helper = MoEHelper.getInstance(context);
        helper.setUniqueId(UUID.randomUUID().toString());
        helper.setEmail(email);
        helper.setFullName(name);
        helper.setFirstName(name);
    }

    static void trackScreenLoadEvent(Context context, String screenName) {
        MoEHelper.getInstance(context).trackEvent(screenName);
    }

    static void clickButtonEvent(Context context, String action, String screenName) {
        PayloadBuilder builder = new PayloadBuilder();
        builder.putAttrString("name", action);
        builder.putAttrString("screen", screenName);
        MoEHelper.getInstance(context).trackEvent("Button Click Action", builder.build());
    }

    static void clickNavEvent(Context context, String action, String screenName) {
        PayloadBuilder builder = new PayloadBuilder();
        builder.putAttrString("name", action);
        builder.putAttrString("screen", screenName);
        MoEHelper.getInstance(context).trackEvent("Navigation Click Action", builder.build());
    }
}
